﻿using UnityEngine;
using System.Collections;

public class Horse : MonoBehaviour {

	private GameManager gameManager;

	public int points = 10;
	public bool stopAnim = false;
	private Animator myAnimator;

	void Start () {
		gameManager = GameObject.Find ("Main").GetComponent<GameManager>();
		myAnimator = GetComponent<Animator> ();
	}
	

	void OnMouseDown () {
		stopAnim = !stopAnim;
		myAnimator.SetBool("Stop", stopAnim);

		StartCoroutine( EndAfterDelay ());
	}

	IEnumerator EndAfterDelay() {
		
		yield return new WaitForSeconds (1);
		gameManager.addPoints(points);
	
	}
}
