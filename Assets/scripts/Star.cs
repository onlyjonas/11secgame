﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(AudioSource))]
public class Star : MonoBehaviour {

	private GameManager gameManager;

	public int points = 1;
	public Sprite deactiveTexture;
	public bool active = true;

	private SpriteRenderer rend;
	private AudioSource audio;

	void Start () {
		gameManager = GameObject.Find ("Main").GetComponent<GameManager>();
		audio = GetComponent<AudioSource>();
		rend = GetComponent<SpriteRenderer> ();
	}

	void OnMouseOver () {
	
		if (active) {
			gameManager.addPoints(points);
			rend.sprite = deactiveTexture;
			audio.Play ();
			active = false;
		}
	}
}
