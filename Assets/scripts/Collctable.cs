﻿using UnityEngine;
using System.Collections;


public class Collctable : MonoBehaviour {

	private GameManager gameManager;

	public int points = 1;
	public AudioClip clip;

	private AudioSource audio;

	void Start () {
		gameManager = GameObject.Find ("Main").GetComponent<GameManager>();
		audio = GetComponent<AudioSource>();
	}

	void OnTriggerEnter2D(Collider2D other) {

		if (other.tag == "Player") {
			Debug.Log ("TRIGGER");
			gameManager.addPoints(points);
			AudioSource.PlayClipAtPoint (clip, Camera.main.transform.position, 1f);
			Destroy (gameObject);
		}
	}

}
