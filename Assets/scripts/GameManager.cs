﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	// SCORE
	public int winScore = 10;
	public int myScore = 0;
	public GameObject textObj;
	private Text scoreTxt;

	// TIMER
	public float gameTime = 5;
	public Transform timerTransform;

	private float currentTime;
	private Vector2 defaultSpriteScale;


	void Start () {

		// init timer
		currentTime = gameTime;
		defaultSpriteScale = timerTransform.localScale;

		// score
		scoreTxt =  textObj.GetComponent<Text>(); 

	}


	void Update () {
		
		// check timer
		if (currentTime > 0) {

			// update timer
			currentTime -= Time.deltaTime;
			UpdateSpriteScale();
		
		} else {
			
			Debug.Log("GameEnd");

			// check score
			if (myScore >= winScore) {
				SceneManager.LoadScene ("gameWon");
			} else {
				SceneManager.LoadScene ("gameOver");
			}

		}

		if (myScore >= winScore) 
			SceneManager.LoadScene ("gameWon");

		// update score text
		scoreTxt.text = myScore + "/" + winScore;

	}


	void UpdateSpriteScale () {

		float spriteWidth = defaultSpriteScale.x * (currentTime / gameTime);

		Vector2 newScale = new Vector2 (spriteWidth, defaultSpriteScale.y);

		timerTransform.localScale = newScale;
	
	}


	public void addPoints (int points) {
		myScore += points;
	}



}

