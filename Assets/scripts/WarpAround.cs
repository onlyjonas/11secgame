﻿using UnityEngine;
using System.Collections;

public class WarpAround : MonoBehaviour {

	public float minHoriz = -5; 
	public float maxHoriz = 5;
	public float minVert = -5;
	public float maxVert = 5;

	void Update () {
		// Warp Around
		Vector3 newPos = transform.position;

		if(transform.position.x < minHoriz) newPos.x = maxHoriz;
		if(transform.position.x > maxHoriz) newPos.x = minHoriz;
		if(transform.position.y < minVert) newPos.y = maxVert;
		if(transform.position.y > maxVert) newPos.y = minVert;

		transform.position = newPos;
	}
}
