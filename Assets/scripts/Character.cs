﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

	private Vector3 moveDirection;
	public float speed = 5.0f;

	void Update () {
	
		// Move
		moveDirection = new Vector3 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"), 0);
		moveDirection= transform.TransformDirection (moveDirection);
		moveDirection *= speed;

		transform.Translate (moveDirection * Time.deltaTime, Space.Self);
	

	}

}